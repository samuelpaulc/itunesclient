Running project:
- npm install
- npm run start
- open localhost:8080 to see the app

Brief description:
Basic implementation to search artists from Itunes and albums associated with artist

Used redux as store, whatwg-fetch for data fetch along with thunk middleware for async operations
Used react-select library for editable combo box

Also basic responsiveness has been added
