Webpack can
- bundle together many js files into 1 big bundle
- convert code from 1 form to another. Ex: ES6 -> ES5
- bundle together css files into  big css files
- Via css-loader provide unique class names for style classes based on the file they are in

Properties:

entry
- With this property we can tell webpack the starting js file
- we can have multiple entries, each of which will generate a js file

ouput
--------
filename:
- configure the output js file name
- we can give explicitly like bundle, but that will work only if have 1 bundle
- Better to provide using standard formats like '[name].js'. This will make a bundle for each entry

path
- this is the folder where to put the bundled js and css files

publicPath
- this is the folder where the generated css and js files will be 'served' from when we use hot relaoding
- Hot reloading is automatic compiling when we make and save files, and these changes will be calculated and delivered from memory

module
rules:
Specify how we want webpack to handle css and js files
