    
import {
    ARTIST_SELECTED, ALBUMS_RECEIVED, ADD_TO_FAVOURITES, REMOVE_FROM_FAVOURITES, FILTER_FAVOURITES_BY_NAME
} from '../components/actionTypes';

const defaultState = {
    selectedArtistName: '',
    selectedArtistId: '',
    favouriteAlbumIds: {},
    alBumIdToArtistIdMap: {},
    artists: {},
    albums: {}
};

const rootReducer = (state = defaultState, action) => {
    const { type, payload } = action;
    if (type === ARTIST_SELECTED) {
        const { label: selectedArtistName, value: selectedArtistId } = payload; 
        return {
            ...state,
            selectedArtistName,
            selectedArtistId,
            artists: {
                ...state.artists,
                [selectedArtistId]: selectedArtistName
            }
        };
    }

    if (type === ALBUMS_RECEIVED) {
        const 
            artistId = payload.results[0].artistId,
            albums = payload.results.slice(1),
            alBumIdToArtistIdMap = albums.reduce((acc, album) => {
                acc[album.collectionId] = artistId;
                return acc;
            }, { ...state.alBumIdToArtistIdMap });
        return {
            ...state,
            albums: {
                ...state.albums,
                [artistId]: albums
            },
            alBumIdToArtistIdMap
        };
    }
    
    if (type === ADD_TO_FAVOURITES) {
        return {
            ...state,
            favouriteAlbumIds: {
                ...state.favouriteAlbumIds,
                [payload]: true
            }
        };
    }

    if (type === REMOVE_FROM_FAVOURITES) {
        const favouriteAlbumIds = {...state.favouriteAlbumIds};
        delete favouriteAlbumIds[payload];
        return {
            ...state,
            favouriteAlbumIds
        };
    }

    if (type === FILTER_FAVOURITES_BY_NAME) {
        return {
            ...state,
            filter: action.payload.toLowerCase()
        };
    }

    return state;
};

export default rootReducer;