import { songsUrl } from './constants';
import {
    ARTIST_SELECTED, ALBUMS_RECEIVED, ADD_TO_FAVOURITES, REMOVE_FROM_FAVOURITES, FILTER_FAVOURITES_BY_NAME
} from './actionTypes';

export const artistSelected = function(artist) {
    return {
        type: ARTIST_SELECTED,
        payload: artist
    };
};

const albumsReceived = albums => ({ type: ALBUMS_RECEIVED, payload: albums })

export const getAlbumsUrl = artistId => `https://itunes.apple.com/lookup?entity=album&id=${artistId}`;

// action creator returning a function
export const getAlbums = (artistId) => {
    return function(dispatch) {
        fetch(getAlbumsUrl(artistId))
        .then(function(response) { return response.json() })
        .then(albums => dispatch(albumsReceived(albums)))
        // .catch(error => dispatch(songsFetchFailed(error)))
    };
};

export const addToFavourites = albumId => ({ type: ADD_TO_FAVOURITES, payload: albumId });
export const removeFromFavourites = albumId => ({ type: REMOVE_FROM_FAVOURITES, payload: albumId });
export const filterFavourtiesByName = filter => ({ type: FILTER_FAVOURITES_BY_NAME, payload: filter })