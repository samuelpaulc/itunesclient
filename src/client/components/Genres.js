import React from 'react';
import styles from './App.css';
import { GENRES } from './constants';

const genres = GENRES.map(function(genre, idx) {
    return (
        <div key={idx} className={styles.genre}>{genre}</div>
    );
});

const Genres = (props) => {
    return (
        <div className={styles.genres}>
            {genres}
        </div>
    );
}

export default Genres;