import React from 'react';
import styles from './ContentContainer.css';
import LeftPanel from './LeftPanel.js';
import ArtistInfo from './ArtistInfo.js';

const ContentContainer = (props) => {
    return (
        <div className={styles.container}>
            <LeftPanel {...props}/>
            <ArtistInfo {...props}/>
        </div>
    );
}

export default ContentContainer;