import React from 'react';
import { Async } from 'react-select';

import styles from './LeftPanel.css';
import { artistSelected, getAlbums } from './actionCreators';
import Favourites from './Favourites';

class LeftPanel extends React.Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    getArtists(input) {
        if (!input) {
			return Promise.resolve({ options: [] });
		}

		return fetch(`https://itunes.apple.com/search?entity=musicArtist&term=${encodeURIComponent(input)}`)
		.then((response) => response.json())
		.then((json) => {
			return { 
                options: json.results.map(artist => {
                    return { label: artist.artistName, value: artist.artistId };
                }),
                complete: true
             };
		});
    }

    onChange(selectedArtist) {
        const { selectedArtistName, selectedArtistId, favouriteArtists, dispatch,albums } = this.props;
        dispatch(artistSelected(selectedArtist));
        if (!albums[selectedArtist.value]) {
            // fetch albums for artist if not already in store
            // TODO handle side effects in custom middleware
            dispatch(getAlbums(selectedArtist.value));
        }
    }

    render() {
        const { selectedArtistName, selectedArtistId, favouriteAlbumIds, dispatch, albums, alBumIdToArtistIdMap } = this.props;
        const selectedArtist = selectedArtistName && { label: selectedArtistName, value: selectedArtistId };
        return (
            <div className={styles.container}>
                <Async 
                    placeholder="Search for artists here"
                    noResultsText="No match!"
                    loadOptions={this.getArtists}
                    onChange={this.onChange}
                    value={selectedArtist}
                />
                <Favourites {...this.props} />
            </div>
        );
    }
}

export default LeftPanel;
0