import React from 'react';

import styles from './Albums.css';
import Card from './AlbumCard';

class Albums extends React.Component {
    render() {
        const 
            { albums, selectedArtistId, favouriteAlbumIds, dispatch } = this.props,
            albumsForArtist = albums[selectedArtistId];

        if (!albumsForArtist) {
            return (
                <div className={styles.container}>
                    <div className={styles.label}>Albums</div>
                </div>
            );
        }

        return (
            <div className={styles.container}>
                <div className={styles.label}>Albums</div>
                <div>
                    {albumsForArtist.map((album, idx) => (<Card key={idx} {...album} isFavourite={favouriteAlbumIds[album.collectionId]} dispatch={dispatch} />))}
                </div>
            </div>
        );
    }
}

export default Albums;