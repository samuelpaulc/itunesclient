import React from 'react';

import styles from './Favourites.css';
import Cancel from 'svg-react-loader?name=Icon!../svg/cancel.svg';
import { removeFromFavourites, filterFavourtiesByName } from './actionCreators';

class Favourites extends React.Component {
    render() {
        const
            { albums, favouriteAlbumIds, alBumIdToArtistIdMap, filter, dispatch } = this.props,
            keys = Object.keys(favouriteAlbumIds),
            filteredFavourites = [];

        Object.keys(favouriteAlbumIds).forEach((albumId, idx) => {
            const artistId = alBumIdToArtistIdMap[albumId];
            const album = albums[artistId].find(album => (album.collectionId == albumId));
            if (filter && album.artistName.toLowerCase().indexOf(filter) === -1) {
                return;
            }
            filteredFavourites.push(
                <div key={idx} className={styles.artistInfo}>
                    <Cancel className={styles.cancel} onClick={() => dispatch(removeFromFavourites(albumId))}/>
                    <div className={styles.albumName}>{album.collectionName} by {album.artistName}</div>
                </div>
            );
        });
        return (
            <div className={styles.container}>
                <div className={styles.label}>Favourites</div>
                { keys.length ? 
                    (
                        [
                            <div key="filter">
                                <div>Filter</div>
                                <input
                                    placeholder="Type artist name here"
                                    onChange={e => dispatch(filterFavourtiesByName(e.target.value))}
                                    style={{ margin: '5px 0', width: '100%' }}
                                />

                            </div>,
                            <div key="list" className={styles.favouritesList}>
                                {filteredFavourites}
                            </div>
                        ]
                    ) : null
                }
            </div>
        );
    }
}

export default Favourites;