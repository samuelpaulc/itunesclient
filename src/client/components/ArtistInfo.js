import React from 'react';
import styles from './ArtistInfo.css';

import Albums from './Albums';

const ArtistInfo = (props) => {
    return (
        <div className={styles.container}>
            <Albums {...props} />
        </div>
    );
}

export default ArtistInfo;