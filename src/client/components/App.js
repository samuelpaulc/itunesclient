import React from 'react';
import { connect } from 'react-redux';
import 'whatwg-fetch';

import styles from './App.css';
import Header from './Header';
import ContentContainer from './ContentContainer';

const App = (props) => {
    const { dispatch } = props;
    
    return (
        <div className={styles.container} >
            <Header />
            <ContentContainer {...props} />
        </div>
    );
}

const mapStateToProps = (state) => ({ ...state });

export default connect(mapStateToProps)(App);