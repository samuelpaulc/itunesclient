import React from 'react';
import styles from './App.css';

const Header = (props) => {
    return (
        <div className={styles.header}>
            iTunes - Artists, Songs, and more...
        </div>
    );
}

export default Header;