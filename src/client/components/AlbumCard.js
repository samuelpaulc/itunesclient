import React from 'react';

import styles from './Albums.css';
import Heart from 'svg-react-loader?name=Icon!../svg/heart.svg';
import { addToFavourites, removeFromFavourites } from './actionCreators';

export default (props) => {
    const { collectionId, artworkUrl100, isFavourite, dispatch } = props;
    return (
        <div className={styles.card} >
            <img 
                src={props.artworkUrl100}  
                style={{ width: '110px' }} 
                title={props.collectionName}
            />
            <div className={styles.albumNameContainer}>
                <div 
                    title={isFavourite ? 'Remove from favorites' : 'Add to favourites'}
                    onClick={() => dispatch(isFavourite ? removeFromFavourites(collectionId): addToFavourites(collectionId))}
                >
                    <Heart className={styles.heart} style={{ fill: props.isFavourite ? 'yellow' : 'gray' }} />
                </div>
                <span className={styles.albumName}>{props.collectionName}</span>
            </div>
        </div>
    );
}
