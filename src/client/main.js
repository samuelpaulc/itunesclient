import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';

import store from './redux/createStore';
import App from './components/App';
import './reset.css';

ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);